var AppDispatcher = require('../dispatcher/AppDispatcher');
var UserConstants = require('../constants/UserConstants');

var UserActions = {

  /**
   * @param  {string} text
   */
  save: function(userId) {
    AppDispatcher.dispatch({
      actionType: UserConstants.USER_SAVE,
      userId:userId
    });
      
    console.log("UserActions.save("+userId+")");
  }

};

module.exports = UserActions;