/** @jsx React.DOM */

var React   = require('react');
var Profile = require('./profile.jsx');

var profiles = [{"name":"david","id":1},{"name":"jeff","id":2}];

React.renderComponent(
    <Profile
        profiles={profiles}
    />, document.body);