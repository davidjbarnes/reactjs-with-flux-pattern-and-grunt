/** @jsx React.DOM */

var React = require('react');
var ProfileItem = require('./profile_item.jsx');

module.exports = React.createClass({
    render: function() {
        return (
            <div className="Profile">
                {this.props.profiles.map(function(item, index) {
                    return <ProfileItem item={item} />;
                })}
            </div>
        )
    }
});



//<p onClick={this.profileClicked.bind(this, item.name)}>{item.name}{item.index}</p>
//                <p><a onClick={this.profileClicked.bind(this, this.props.name)}>{this.props.name}</a></p>
//                <p><a onClick={this.profileClicked.bind(this, this.props.name2)}>{this.props.name2}</a></p>
//               <h3>{this.state.profile}</h3>