/** @jsx React.DOM */

var React = require('react');
var UserActions = require('../actions/UserActions');

module.exports = React.createClass({
    getInitialState: function() {
        return {name:null};
    },
    onClick: function (item) {
        UserActions.save(item.id)
        this.setState({name:item.name});
    },
    render: function() {
        var item = this.props.item;
        
        return (
            <div className="ProfileItem">
                <p onClick={this.onClick.bind(this, item)}>{item.name}</p>
            </div>
        )
    }
});