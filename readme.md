# A Simple Flux and ReactJS Example #

The Flux design pattern has three core parts: the dispatcher, the stores, and the views. The purpose of the this application is to show the simplicity of the design pattern. Mentally breaking away from an MVC approach was challenging at first. However, Flux is a far more natural and scalable approach for ReachJS applications.

## Build and run this example: ##

```
#!javascript

$ git clone [this repo]
$ cd flux-example
$ npm install
$ grunt watch
```

Launch index.html in browser.